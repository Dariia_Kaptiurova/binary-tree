package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class BsTreeRTest {

    BsTreeR test = new BsTreeR();

    @BeforeEach
    public void createTestTree() {
        test.add(new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv"));
        test.add(new Person(2, "Nikita", "Smirnov", 22, "Kyiv"));
        test.add(new Person(3, "Andrew", "Pikozh", 22, "Kyiv"));
    }

    @Test
    void clear() {

        //given

        //when
        test.clear();

        //then
        assertEquals(0, test.size());
        assertNotEquals(10, test.size());
    }

    @Test
    void size() {

        //given

        //when
        int expected = 3;

        //then
        assertEquals(expected, test.size());
        assertNotEquals(0, test.size());
    }

    @Test
    void toArray() {
    }

    @Test
    void add() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //when
        int expected = 4;
        test.add(person);

        //then
        assertEquals(expected, test.size());
        assertNotEquals(10, test.size());
    }

    @Test
    void del() {
    }

    @Test
    void getWidth() {
    }

    @Test
    void getHeight() {
    }

    @Test
    void nodes() {
    }

    @Test
    void leaves() {
    }

    @AfterEach
    public void cleanUpStreams() {
        System.setOut(null);
    }
}