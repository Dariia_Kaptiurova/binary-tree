package com.company;

public class Node {

    Person value;
    Node left;
    Node right;

    Node(Person value) {
        this.value = value;
        right = null;
        left = null;
    }
}

