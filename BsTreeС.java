package com.company;

import java.util.List;

public class BsTreeС implements BSTree {

    private Node root;

    public BsTreeС() {
    }


    public BsTreeС(List<Person> people) {
        if (people != null) {
            for (Person value : people) {
                add(value);
            }
        }
    }


    @Override
    public void clear() {
        root = null;
    }

    @Override
    public int size() {
        return getSizeOfTree(root);
    }

    public int getSizeOfTree(Node root) {
        if (root == null) {
            return 0;
        }
        return getSizeOfTree(root.left) + getSizeOfTree(root.right) + 1;
    }

    @Override
    public Person[] toArray() {
        return new Person[0];
    }

    @Override
    public void add(Person val) {

    }

    @Override
    public Person del(Person val) {
        return null;
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }
}
