package com.company;

public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private String city;

    public Person() {
    }

    public Person(int idUser, String firstNameUser, String lastNameUser, int ageUser, String cityUser) {
        id = idUser;
        firstName = firstNameUser;
        lastName = lastNameUser;
        age = ageUser;
        city = cityUser;
    }

    // Блок геттеров
    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    // Блок сеттеров
    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "(id: " + id + "): "
                + firstName +
                " " + lastName +
                ", возраст " + age + ", город "
                + city + ";" + "\n";
    }
}
