package com.company;

import java.util.ArrayList;
import java.util.List;

public class BsTreeR implements BSTree {

    private Node root;

    public BsTreeR() {
    }


    public BsTreeR(List<Person> people) {
        if (people != null) {
            for (Person value : people) {
                add(value);
            }
        }
    }


    @Override
    public void clear() {
        root = null;
    }

    @Override
    public int size() {
        return getSizeOfTree(root);
    }

    public int getSizeOfTree(Node root) {
        if (root == null) {
            return 0;
        }
        return getSizeOfTree(root.left) + getSizeOfTree(root.right) + 1;
    }

    @Override
    public Person[] toArray() {
        return new Person[getSizeOfTree(root)];
    }

    @Override
    public void add(Person val) {
        root = addRecursive(root, val);
    }

    private Node addRecursive(Node current, Person val) {
        if (current == null) {
            return new Node(val);
        }
        if (val.getFirstName().compareTo(current.value.getFirstName()) != 0) {
            current.left = addRecursive(current.left, val);
        } else if (val.getFirstName().compareTo(current.value.getFirstName()) == 0) {
            current.right = addRecursive(current.right, val);
        } else {
            return current;
        }

        return current;
    }

    @Override
    public Person del(Person val) {
        return val;
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }
}
